$(document).ready(function(){

    var selectedClass = "";
    $(".fil-cat").click(function(){
            selectedClass = $(this).attr("data-rel");
            $("#portfolio-list").fadeTo(100);
            $("#portfolio-list .col-md-3").not("."+selectedClass).fadeOut().removeClass('scale-anm');
            setTimeout(function() {
                $("."+selectedClass).fadeIn().addClass('scale-anm');
                $("#portfolio-list").fadeTo(300);
            }, 300);

        });

    $('body').on('click', '.homework-block', function(){
        var id = $(this).attr('data-id');
        if($(this).attr('id') == 'active')
        {
            $.ajax({
                url: '/homework/gethomework/',
                type: 'get',
                contentType: 'application/x-www-form-urlencoded',
                data:'id='+id,
                success: function( data, textStatus, jQxhr ){
                    $('#model').html(data);
                    $('#portfolioModal').modal('show');
                },
                error: function( jqXhr, textStatus, errorThrown ){
                    console.log( errorThrown );
                }
            });
        }
    });

    $('body').on('click', '.homework-done-button', function(e){
        var id = $('#homework-id').val();

        $.ajax({
            url: '/homework/done',
            type: 'get',
            contentType: 'application/x-www-form-urlencoded',
            data:'id='+id+'&status=DONE',
            success: function( data, textStatus, jQxhr ){
                location.reload();
            },
            error: function( jqXhr, textStatus, errorThrown ){
                console.log( errorThrown );
            }
        });
    });

    $('body').on('click', '#homework-add-btn', function(e){
        var title = $('#title').val();
        var description = $('#description').val();
        var points = $('#points').val();
        var duration = $('#duration').val();

        $.ajax({
            url: '/homework/addhomeworkAjax',
            type: 'get',
            contentType: 'application/x-www-form-urlencoded',
            data:'title='+title+'&description='+description+'&points='+points+'&duration='+duration,
            success: function( data, textStatus, jQxhr ){
                location.reload();
            },
            error: function( jqXhr, textStatus, errorThrown ){
                console.log( errorThrown );
            }
        });
    });

    $('body').on('click', '#homework-add', function(e){

        $.ajax({
                url: '/homework/addhomework/',
                type: 'get',
                contentType: 'application/x-www-form-urlencoded',
                data:'id='+1,
                success: function( data, textStatus, jQxhr ){
                    $('#model_parent').html(data);
                    $('#portfolioModalParent').modal('show');
                    console.log( "Hi" );
                },
                error: function( jqXhr, textStatus, errorThrown ){
                    console.log( errorThrown );
                }
            });
    });

    $('.redeem').on('click',function(){
        var id = $(this).attr('data-id');

        $.get( "/gift/redeem/",
        {
            id: id
        })
        .done(function( data ) {
            data = data.split("-");
            console.log(data[0]+data[1]);
            if(data[0] == "true"){
                $(".message").empty();
                $("#"+data[2]).remove();
                $(".message").append('<div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> Successfully redeemed</em></div>');
                $("#points").html(data[1]);
            }else{
                $(".message").empty();
                $(".message").append('<div class="alert alert-danger"><span class="glyphicon glyphicon-ok"></span><em> You need '+data+' more points</em></div>');
                
            
            }
            
        });
    });

    $('body').on('click', '.homework-completed-popup', function(e){
        var id = $(this).attr('data-id');
        if(!$('.homework-completed-button').length) {
            $(this).find('.homework-desc-inner').append('<a href="#" data-id="' + id + '" class="homework-completed-button">Complete</a>');
        }
    });

    $('body').on('click', '.homework-completed-button', function(e){
        var id = $(this).attr('data-id');
        $.ajax({
            url: '/parents/complete',
            type: 'get',
            contentType: 'application/x-www-form-urlencoded',
            data:'id='+id,
            success: function( data, textStatus, jQxhr ){
                location.reload();
            },
            error: function( jqXhr, textStatus, errorThrown ){
                console.log( errorThrown );
            }
        });
    });


});