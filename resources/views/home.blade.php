@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if(isset($todoHomework))
                        @foreach($todoHomework as $homework)
                            <a href="#" data-id="{{$homework->id}}" class="homework-done-button">
                                {{$homework->title}}
                            </a>
                        @endforeach
                    @else
                        Sorry no records
                    @endif
                </div>

                <div class="panel-body">
                    @if(isset($completedHomework))
                        @foreach($completedHomework as $homework)
                            {{$homework->title}}
                        @endforeach
                    @else
                       Sorry no records
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
