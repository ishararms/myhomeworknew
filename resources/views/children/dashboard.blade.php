@extends('layouts.app')

@section('content')
<div class="wapper kid-dashboard">
    <div class="row">
        <div class="col-md-3 left-col">>
            <img src="img/logo.png" class="logo">
            <div class="gifts">
            <div class="message"></div>
    

                @if($gifts)
                    @foreach($gifts as $gift)
                        <div class="gift" id = "{{$gift->id}}" >
                            <img src="img/{{ $gift->image }}" style="width: 100px;" class="pull-left">
                            <div class="pull-right">
                                <h1 class="title">{{ $gift->name }}</h1>
                                <h1 class="points">{{ $gift->points }} pts</h1>
                                <a href="#" data-id="{{$gift->id}}" class="redeem">Redeem</a>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
            <div class="opration-buttons">
                <a href="{{ url('/logout') }}" class="logout">Logout</a>
            </div>
        </div>
        <div class="col-md-9 right-col">
            <div class="child-details">
                <div class="col-md-8">
                    <img src="{{$user->picture}}" style="width: 200px;" class="pull-left profile-img">
                    <div class="pull-left username-col">
                        <h2>{{$user->name}}</h2>
                        <h3 id="points">{{$user->total_points}} pts</h3>
                    </div>
                </div>
                <div class="col-md-4">
                    <div id="clock" class="light">
                        <div class="display">
                            <div class="weekdays"></div>
                            <div class="ampm"></div>
                            <div class="alarm"></div>
                            <div class="digits"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="homework-list">
                @if($todoHomework)
                    <?php $i = 0 ?>
                    @foreach($todoHomework as $homework)
                        <div data-id="{{$homework->id}}" class="homework-block col-md-3" @if($i == 0) id="active" @endif>
                            <div class="homework-desc">
                                {{$homework->title}}
                            </div>
                            <img src="img/cabin.png" alt="cabin" style="max-width: 400px; width: 100%">
                        </div>
                        <?php $i++ ?>
                    @endforeach
                @else
                    Sorry no records
                @endif
            </div>
        </div>
    </div>
</div>
<div id="model"></div>

<div class="modal fade" id="redeem" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <!--<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>-->
            <div class="modal-body">
                Are you Sure? Do you want to proceed with redeem
            </div>
            <!--<div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>-->
        </div>
    </div>
</div>
@endsection


