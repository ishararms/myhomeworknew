@extends('layouts.app')

@section('content')
<img src="img/logo.png" style="position: absolute; width: 800px; margin-left: 28%">
<div class="loign-wapper">
    <div id="clouds">
        <div class="cloud x1"></div>
        <!-- Time for multiple clouds to dance around -->
        <div class="cloud x2"></div>
        <div class="cloud x3"></div>
        <div class="cloud x4"></div>
        <div class="cloud x5"></div>
    </div>

    <div class="container" style="margin-top: 6%">
        <div class="container" style="margin-top: 6%">
            <div id="login" >
                    <fieldset class="clearfix">
                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                            {{ csrf_field() }}
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <div class="col-md-12">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <div class="col-md-12">
                                    <input id="password" type="password" class="form-control" name="password">

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-12 ">
                                    <button type="submit" id="submit" class="btn btn-primary">
                                        GO
                                    </button>
                                </div>
                            </div>
                        </form>
                    </fieldset>
            </div> <!-- end login -->
        </div>
        <div class="footer" style="text-align: center; margin-top: 25%">Solution By Spinal Krushers</div>
    </div>
</div>
@endsection
