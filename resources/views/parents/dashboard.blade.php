@extends('layouts.app')

@section('content')
    <div class="wapper parent-dashboard">
        <div class="row">
            <div class="col-md-2 left-col">>
                <img src="../img/logo.png" class="logo">
                <div class="nav">
                    <ul>
                        <li><a hre="#">Reports</a></li>
                        <li><a hre="#">Buy Gifts</a></li>
                    </ul>
                </div>
                <div class="opration-buttons">
                    <a href="{{ url('/logout') }}" class="logout">Logout</a>
                </div>
            </div>
            <div class="col-md-10 right-col">
                <div class="parent-menu-bar">
                    <div class="col-md-3" id = "summary">
                        <a href="#">Summary</a>
                    </div>
                    <div class="col-md-3" id = "homework-add">
                        <a href="#">Add homework</a>
                    </div>
                    <div class="col-md-6">
                        <img src="../{{$user->picture}}" class="pull-left profile-img">
                        <div class="pull-left username-col">
                            <h2>{{$user->name}}</h2>
                        </div>
                        <a href="#" class="reminder-button pull-right">Reminder</a>
                    </div>
                </div>
                <div class="homework-list">
                    @if($doneHomework)
                        <ul id="portfolio-filter">
                            <li><a class="fil-cat" data-rel="ALL">All</a></li>
                        <?php $status = ''; ?>
                        @foreach($doneHomework as $homework)
                            @if($status != $homework->status)
                            <li><a class="fil-cat" title="" data-rel="{{$homework->status}}">{{$homework->status}}</a></li>
                            @endif
                            <?php $status = $homework->status; ?>
                        @endforeach
                        </ul>
                    @endif

                    @if($doneHomework)
                        <div id="portfolio-list">
                        @foreach($doneHomework as $homework)
                            @if($homework->status == "ACTIVE")
                            <div data-id="{{$homework->id}}" class="col-md-3 scale-anm {{$homework->status}} ALL">
                                <div class="homework-block">
                                    <div class="homework-desc">
                                        <img src="../img/cabin.png" alt="cabin" style="max-width: 150px; width: 100%" class="pull-left">
                                        <div class="homework-desc-inner">
                                            <h2>{{$homework->title}}</h2>
                                            <span>{{$homework->status}}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                            @if($homework->status == "DONE")
                            <div data-id="{{$homework->id}}" class="col-md-3 homework-completed-popup scale-anm {{$homework->status}} ALL">
                                <div class="homework-block">
                                    <div class="homework-desc">
                                        <img src="../img/cabin.png" alt="cabin" style="max-width: 150px; width: 100%" class="pull-left">
                                        <div class="homework-desc-inner">
                                            <h2>{{$homework->title}}</h2>
                                            <span>{{$homework->status}}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                            @if($homework->status == "COMPLETED")
                            <div data-id="{{$homework->id}}" class="col-md-3 scale-anm {{$homework->status}} ALL">
                                <div class="homework-block">
                                    <div class="homework-desc">
                                        <img src="../img/cabin.png" alt="cabin" style="max-width: 150px; width: 100%" class="pull-left">
                                        <div class="homework-desc-inner">
                                            <h2>{{$homework->title}}</h2>
                                            <span>{{$homework->status}}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                        @endforeach
                        </div>
                    @else
                        <h1>Sorry no homework to show :(</h1>
                    @endif
                    
                </div>

            </div>
        </div>
    </div>
<div id="model_parent"></div>
@endsection


