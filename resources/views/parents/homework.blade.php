<div class="portfolio-modal modal fade" id="portfolioModalParent" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl">
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="modal-body">
                        <form id="homework-form">
                            <input type="hidden" value="{{$homework->id}}" id="homework-id">
                            <h2 style="font-family:'Handlee', cursive;">{{$homework->title}}</h2>
                            <hr class="star-primary">
                            <img src="img/portfolio/cabin.png" class="img-responsive img-centered" alt="">
                            <p>{{$homework->description}}</p>
                            <!--<ul class="list-inline item-details">
                                <li>Subject:
                                    <strong><a href="https://www.wiziq.com/tutorials/home-science">Home Science</a>
                                    </strong>
                                </li>
                                <li>Date:
                                    <strong><a href="https://www.timeanddate.com/">23 September 2016</a>
                                    </strong>
                                </li>
                                <li>Tutorial:
                                    <strong><a href="https://www.wiziq.com/tutorials/home-science">1</a>
                                    </strong>
                                </li>
                            </ul>-->
                            <button type="button" class="btn btn-default homework-done-button"> Done</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>