<div class="portfolio-modal modal fade" id="portfolioModalParent" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl">
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="modal-body">
                        <form id="homework-form">
                            <input type="hidden" value="{{$homework}}" id="homework-id">
                            <h2 style="font-family:'Handlee', cursive;">Add Homework</h2>
                            <hr class="star-primary">
                            <img src="../img/portfolio/cabin.png" class="img-responsive img-centered" alt="" style="max-width: 500px">
                            
                           <div class="form-group">
    <label for="title">Title</label>
    <input type="text" class="form-control" id="title" placeholder="Title">
  </div>
  <div class="form-group">
    <label for="description">Description</label>
    <textarea class="form-control" id="description" rows="3"></textarea>
  </div>
  <div class="form-group">
    <label for="title">Points</label>
    <input type="text" class="form-control" id="points" placeholder="Points">
  </div>
  <div class="form-group">
    <label for="title">Duration</label>
    <input type="text" class="form-control" id="duration" placeholder="(YYYY:MM:DD HH:MM:SS)">
  </div>
                            <button type="button" class="btn btn-default homework-done-button" id ="homework-add-btn" style="background: #2c3e50"> Add</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>