<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gift extends Model
{
	//get all gifts
	public function get_all(){
		$gifts = \DB::table('gifts')
			->where('status','ACTIVE')
			->get();

		return $gifts;
	}

	//get  gift by id
	public function get_gift_by_Id($id=0){
		$gift = \DB::table('gifts')
			->where(array('id' => $id, 'gifts.status' => 'ACTIVE'))
			->get();

		return $gift;
	}

	//get gifts by parent
	public function get_gifts_by_parent($parentId = 0,$giftId){
		$gifts = \DB::table('gifts')
			->join('parent_gift', 'gifts.id', '=', 'parent_gift.gift_id')
			->where('parent_gift.parent_id', $parentId)->where( 'gifts.status' ,'ACTIVE');

		if(isset($giftId)&&($giftId>0)){
			$gifts->where('gifts.id', $giftId);
		}

		return $gifts->get();
	}

	public function get_first_three_gifts($id){

		$gift = \DB::table('gifts')
            ->join('parent_gifts', 'gifts.id', '=', 'parent_gifts.gift_id')
            ->where('parent_gifts.parent_id' , $id)
			->where('gifts.status' , 'ACTIVE')
			->limit(3)
			->get();

		return $gift;
	}

	public function redeem($data = array()){

		
	}
	
}
