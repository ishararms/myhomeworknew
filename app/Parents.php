<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Parents extends Model
{

    public function get_all(){
        $parents = \DB::table('parent')
            ->where('status','ACTIVE')
            ->get();

        return $parents;
    }

    public function get_parent_by_id($id=0){
        $parents = \DB::table('parent')
            ->where('status','ACTIVE')
            ->where('id',$id)
            ->get();

        return $parents;
    }
    
}
