<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\AuthInterface;
use Illuminate\Http\Request;
use App\Homework;
use Auth;

class HomeworkController extends Controller
{
    protected $request;

    protected $homework;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Homework $homework, Request $request )
    {
        $this->middleware('auth');
        $this->request = $request;
        $this->homework = $homework;
    }

    public function ajaxHomework()
    {

        $homework = $this->homework->getHomework(\Request::get('id'))[0];
        return View('models.homework', compact(
            'homework'
        ));
    }

    public function ajaxHomeworkAdd()
    {

        $homework = "aa";
        return View('models.homeworkadd', compact(
            'homework'
        ));
    }

    public function addhomeworkAjax()
    {
        //dd(\Request::get('title'));
        $this->homework->title = \Request::get('title');
        $this->homework->description = \Request::get('description');
        $this->homework->points = \Request::get('points');
        $this->homework->duration = \Request::get('duration');
        $this->homework->status = "ACTIVE";
        $this->homework->parent_id = Auth::user()->id;
        $this->homework->save();
        
    }

    

    public function doneHomework()
    {
        $data = \Request::all();
        $this->homework->doneHomework($data);
    }

}
