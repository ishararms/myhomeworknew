<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\AuthInterface;
use Illuminate\Http\Request;
use App\Homework;
use App\Parents;
use App\User;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;

class ParentController extends Controller
{
    protected $request;
    protected $parent;
    protected $homework;
    protected $user;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Homework $homework, Request $request, Parents $parent, User $user)
    {
        $this->middleware('auth');
        $this->request = $request;
        $this->homework = $homework;
        $this->parent = $parent;
        $this->user = $user;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $user = Auth::user();
        $doneHomework = $this->homework->getAllHomeworkByParentId($user->id);

        return View('parents.dashboard', compact(
            'user',
            'doneHomework'
        ));
    }
    
    public function completeHomeWork()
    {
        $homeWork = $this->homework->getDoneHomework(1)[0];
        //dd($homeWork);
        if($homeWork){
            if(($homeWork->end_time)<($homeWork->duration)) {
                $homeWork->status = 'COMPLETED';
                if ($this->homework->completeHomework($homeWork)) {
                    //calculate total points for the child (add part)
                    $total_points = ($this->homework->getTotalPoints($homeWork->user_id));
                    $user = $this->user->updatePoints($homeWork->user_id, $total_points);
                }
                return 'true';
            }else{
                $homeWork->status = 'COMPLETED';
                if ($this->homework->completeHomework($homeWork)) {
                    //calculate total points (reduce part)
                    $total_points = ($this->homework->getTotalPoints($homeWork->user_id))-$homeWork->points;
                    $user = $this->user->updatePoints($homeWork->user_id, $total_points);
                }
                return 'true';
            }
        }
            return 'false';

    }
    
}
