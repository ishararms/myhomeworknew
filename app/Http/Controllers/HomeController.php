<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\AuthInterface;
use App\Gift;
use Illuminate\Http\Request;
use App\Homework;
use Auth;

class HomeController extends Controller
{
    protected $request;
    protected $gift;
    protected $homework;
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct(Homework $homework, Request $request ,Gift $gift)
    {
        $this->middleware('auth');
        $this->request = $request;
        $this->homework = $homework;
        $this->gift = $gift;
    }

    /**
     * Show the application dashboard.
     *git 
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $todoHomework = $this->homework->getTodoHomework($user->id);
        $gifts = $this->gift->get_first_three_gifts($user->parent_id);
        $completedHomework = $this->homework->getCompletedHomework($user->id);

        return View('children.dashboard', compact(
            'user',
            'todoHomework',
            'completedHomework',
            'gifts'
        ));
    }
    
    
}
