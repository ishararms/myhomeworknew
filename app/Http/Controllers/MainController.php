<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\AuthInterface;
use Illuminate\Http\Request;
use App\Homework;
use Auth;

class MainController extends Controller
{
    protected $request;

    protected $homework;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Homework $homework, Request $request )
    {
        $this->middleware('auth');
        $this->request = $request;
        $this->homework = $homework;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        if ($user->name == "mom") {
            return redirect('/parents/listHomework');
        } else {
            return redirect('/kids');
        }
    }
    
}
