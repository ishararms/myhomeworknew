<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Gift;
use Auth;

class GiftController extends Controller
{
    Protected $gift;

    public function __construct(Gift $gift)
    {
        $this->middleware('auth');
        $this->gift = $gift;
    }

    public function getAllGifts(){
        return $this->gift->get_all();
    }

    public function getGift($id=0){
        return $this->gift->get_gift_by_Id($id);
    }

    public function getGiftsByParent($parentId = 0, $giftId = 0){
        return $this->gift->get_gifts_by_parent($parentId, $giftId);
    }

    public function ajaxRedeem(){
        $user = Auth::user();

        $data = [
            'id' => \Request::get('id'),
            'user' => $user->id
        ];

        $redeem = $this->gift->find(\Request::get('id'));
        
        if($user->total_points > $redeem->points){
            $redeem->status = 'CLAIMED';
            $redeem->user_id = $data['user'];
            $redeem->save();
            $userBalance = $user->total_points - $redeem->points;
            $user->total_points = $userBalance;
            $user->save();
            $result = "true-".$userBalance."-".$redeem->id;
        }else{
            $userBalance = $redeem->points - $user->total_points;
            $result = $userBalance;
        }
        return $result;
                
    }
}
