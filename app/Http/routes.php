<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::auth();

Route::get('/', 'MainController@index');

//Route::get('/kids', 'HomeController@index');

 Route::get('/kids', [
        'as' => 'kids', 'uses' => 'HomeController@index'
    ]);

Route::get('/parents/listHomework', 'ParentController@index');

Route::get('/parents/complete','ParentController@completeHomeWork');

//homework routes
Route::get('/homework/done', 'HomeworkController@doneHomework');
Route::get('/homework/gethomework', 'HomeworkController@ajaxHomework');
Route::get('/homework/addhomework', 'HomeworkController@ajaxHomeworkAdd');
Route::get('/homework/addhomeworkAjax', 'HomeworkController@addhomeworkAjax');

//gift routes
Route::get('/gift/redeem', 'GiftController@ajaxRedeem');
