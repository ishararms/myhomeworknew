<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Homework extends Model
{
    public function getTodoHomework($id)
    {
        $todoHomeWork = \DB::table('homeworks')
            ->join('user_homeworks', 'homeworks.id', '=', 'user_homeworks.homework_id')
            ->where('homeworks.status', 'ACTIVE')
            ->where('user_homeworks.user_id', $id)
            ->limit(6)
            ->get();

        return ($todoHomeWork) ? $todoHomeWork : false;
    }

    public function getCompletedHomework($id)
    {
        $completedHomeWork = \DB::table('homeworks')
            ->join('user_homeworks', 'homeworks.id', '=', 'user_homeworks.homework_id')
            ->where(array('user_homeworks.user_id' => $id, 'homeworks.status' => 'COMPLETED'))
            ->get();

        return ($completedHomeWork) ? $completedHomeWork : false;
    }

    public function getDoneHomeworkByParentId($id)
    {
        $doneHomeWork = \DB::table('homeworks')
            ->where(array('homeworks.parent_id' => $id, 'homeworks.status' => 'DONE'))
            ->get();

        return $doneHomeWork;
    }

    public function getAllHomeworkByParentId($id)
    {
        $doneHomeWork = \DB::table('homeworks')
            ->where(array('homeworks.parent_id' => $id))
            ->get();

        return $doneHomeWork;
    }

    public function getCompletedHomeworkByParentId($id)
    {
        $completedHomeWork = \DB::table('homeworks')
            ->join('user_homeworks', 'homeworks.id', '=', 'user_homeworks.homework_id')
            ->where(array('homeworks.parent_id' => $id, 'homeworks.status' => 'COMPLETED'))
            ->get();

        return $completedHomeWork;
    }

    public function getDoneHomework($id)
    {
        $doneHomeWork = \DB::table('homeworks')
            ->join('user_homeworks', 'homeworks.id', '=', 'user_homeworks.homework_id')
            ->where(array('user_homeworks.user_id' => $id, 'homeworks.status' => 'DONE'))
            ->get();

        return ($doneHomeWork) ? $doneHomeWork : false;
    }

    public function getHomework($id)
    {
        $homeWork = \DB::table('homeworks')
            ->where(array('homeworks.id' => $id, 'homeworks.status' => 'ACTIVE'))
            ->get();

        return ($homeWork) ? $homeWork : false;
    }

    public function completeHomework($data)
    {
        $homeWork =$this->findOrFail($data->id);
        $homeWork->title = $data->title;
        $homeWork->description = $data->description;
        $homeWork->points = $data->points;
        $homeWork->start_time = $data->start_time;
        $homeWork->end_time = $data->end_time;
        $homeWork->duration = $data->duration;
        $homeWork->status = $data->status;
        $homeWork->parent_id = $data->parent_id;
        $homeWork->updated_at = Carbon::now();
        $saved = $homeWork->save();
        return $saved ? $homeWork : false;
    }

    public function getTotalPoints($id){
        $points = \DB::table('homeworks')
            ->join('user_homeworks', 'homeworks.id', '=', 'user_homeworks.homework_id')
            ->where(array('user_homeworks.user_id' => $id, 'homeworks.status' => 'COMPLETED'))
            ->sum('points');

        return ($points) ? $points : 0;
    }

    public function doneHomework($data = array())
    {
        $homeWork = $this->findOrFail($data['id']);
        $homeWork->status = $data['status'];
        $homeWork->end_time = Carbon::now();

        $saved = $homeWork->save();
        return $saved ? $homeWork : false;
    }
}
