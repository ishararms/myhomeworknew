<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHomeworkTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('homeworks', function(Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('description');
            $table->integer('points');
            $table->dateTime('start_time');
            $table->dateTime('end_time');
            $table->dateTime('duration');
            $table->enum('status', array('ACTIVE', 'INACTIVE', 'DONE', 'COMPLETED'))->default('INACTIVE');
            $table->timestamps('');
            $table->timestamp('deleted_at')->nullable()->default(NULL);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('homework');
    }
}
