-- Adminer 4.1.0 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `gifts`;
CREATE TABLE `gifts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `points` int(11) NOT NULL,
  `status` enum('ACTIVE','INACTIVE','CLAIMED') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'ACTIVE',
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `gifts_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `gifts` (`id`, `name`, `price`, `points`, `status`, `image`, `user_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1,	'Mountain Bicycle',	'7520.00',	1000,	'CLAIMED',	'bike.png',	1,	'2016-09-23 07:23:54',	'2016-09-24 09:39:14',	NULL),
(2,	'HP - cursed child',	'5000.00',	750,	'ACTIVE',	'book1.png',	2,	'2016-09-23 07:23:54',	'2016-09-24 02:16:15',	NULL),
(3,	'Laptop',	'85000',	10000,	'ACTIVE',	'laptop.png',	1,	'2016-09-23 07:23:54',	'2016-09-23 07:23:54',	NULL),
(4,	'family day out',	'10050',	900,	'ACTIVE',	'day-out.jpg',	1,	'2016-09-23 07:23:54',	'2016-09-23 07:23:54',	NULL),
(5,	'Telascope',	'25000',	1050,	'ACTIVE',	'',	2,	'2016-09-23 07:23:54',	'2016-09-23 07:23:54',	NULL);

DROP TABLE IF EXISTS `homeworks`;
CREATE TABLE `homeworks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `points` int(11) NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `duration` datetime NOT NULL,
  `status` enum('ACTIVE','INACTIVE','DONE','COMPLETED') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'INACTIVE',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `homeworks` (`id`, `parent_id`, `title`, `description`, `points`, `start_time`, `end_time`, `duration`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1,	'3',	'Mathematics lesson 1',	'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus fermentum diam ante, ut viverra justo eleifend in. Duis et laoreet est, id mollis dolor 1',	10,	'2016-09-23 07:53:07',	'2016-09-23 21:26:38',	'2016-09-23 07:53:07',	'COMPLETED',	NULL,	'2016-09-23 21:26:38',	NULL),
(2,	'3',	'Mathematics lesson 2',	'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus fermentum diam ante',	10,	'2016-09-23 15:44:05',	'2016-09-23 11:28:09',	'2016-09-23 15:44:05',	'COMPLETED',	NULL,	'2016-09-24 05:43:59',	NULL),
(3,	'3',	'Mathematics lesson 3',	'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus fermentum diam ante, ut viverra justo eleifend in. Duis et laoreet est, id mollis dolor 3',	10,	'2016-09-23 15:45:54',	'2016-09-23 21:29:57',	'2016-09-23 15:45:54',	'COMPLETED',	NULL,	'2016-09-24 05:42:19',	NULL),
(4,	'3',	'Mathematics lesson 4',	'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus fermentum diam ante, ut viverra justo eleifend in. Duis et laoreet est, id mollis dolor',	10,	'2016-09-23 15:47:04',	'2016-09-24 09:15:22',	'2016-09-23 15:47:04',	'DONE',	NULL,	'2016-09-24 09:15:22',	NULL),
(5,	'3',	'Mathematics lesson 5',	'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus fermentum diam ante, ut viverra justo eleifend in. Duis et laoreet est, id mollis dolor',	10,	'2016-09-23 15:48:15',	'2016-09-25 15:00:15',	'2016-09-23 15:48:15',	'ACTIVE',	NULL,	NULL,	NULL),
(6,	'3',	'English Homework 1',	'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus fermentum diam ante, ut viverra justo eleifend in. Duis et laoreet est, id mollis dolor',	15,	'2016-09-24 07:43:06',	'2016-09-25 07:43:06',	'2016-09-24 07:43:06',	'ACTIVE',	NULL,	NULL,	NULL),
(7,	'3',	'English Homework 2',	'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus fermentum diam ante, ut viverra justo eleifend in. Duis et laoreet est, id mollis dolor',	15,	'2016-09-24 07:44:07',	'2016-09-25 07:44:07',	'2016-09-24 07:44:07',	'ACTIVE',	NULL,	NULL,	NULL),
(8,	'3',	'Science lesson 1',	'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus fermentum diam ante',	10,	'2016-09-24 07:46:08',	'2016-09-25 07:46:08',	'2016-09-24 07:46:08',	'ACTIVE',	NULL,	NULL,	NULL),
(9,	'3',	'Science lesson 2',	'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus fermentum diam ante',	20,	'2016-09-24 09:48:39',	'2016-09-25 09:48:39',	'2016-09-24 09:48:39',	'ACTIVE',	NULL,	NULL,	NULL);

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table',	1),
('2014_10_12_100000_create_password_resets_table',	1),
('2016_09_23_033337_create_parent_table',	1),
('2016_09_23_035347_create_homework_table',	1),
('2016_09_23_040856_create_gift_table',	1),
('2016_09_23_041605_create_parent_gift_table',	1),
('2016_09_23_052250_create_user_homework_table',	2),
('2016_09_23_100930_add_parent_id_to_homework_table',	3),
('2016_09_23_214815_update_user_table',	4);

DROP TABLE IF EXISTS `parents`;
CREATE TABLE `parents` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phonenumber` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'ACTIVE',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `parents` (`id`, `first_name`, `last_name`, `email`, `password`, `phonenumber`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1,	'Ishara',	'pubudini',	'ishara@riverviewms.com',	'123',	'0112334445',	'ACTIVE',	NULL,	NULL,	NULL);

DROP TABLE IF EXISTS `parent_gifts`;
CREATE TABLE `parent_gifts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `gift_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `parent_gifts` (`id`, `parent_id`, `gift_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1,	1,	1,	'2016-09-23 09:23:23',	'2016-09-23 09:23:23',	NULL),
(2,	1,	2,	'2016-09-23 09:23:23',	'2016-09-23 09:23:23',	NULL),
(3,	1,	3,	'2016-09-23 09:23:23',	'2016-09-23 09:23:23',	NULL),
(4,	1,	4,	'2016-09-23 09:23:23',	'2016-09-23 09:23:23',	NULL),
(5,	1,	5,	'2016-09-23 09:23:23',	'2016-09-23 09:23:23',	NULL);

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `picture` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'ACTIVE',
  `total_points` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `users` (`id`, `name`, `email`, `username`, `password`, `picture`, `status`, `total_points`, `parent_id`, `remember_token`, `created_at`, `updated_at`) VALUES
(1,	'Amanda Shehani',	'kid@gmail.com',	'',	'$2y$10$LwbLBNwmCe35sW1hm36UEuouCnwmSgFWBLMMuNQChf4e2PzADHmhO',	'img/default-child.png',	'ACTIVE',	'755',	1,	'D4FwiDSDrTsSAPkP4YOAt7NxKZHcd5uoyuZKAurazMIlueX2N2KO2Zdxk5ty',	'2016-09-23 05:33:39',	'2016-09-24 09:50:23'),
(2,	'Aryana Fernando',	'aryana@gmail.com',	'',	'$2y$10$LwbLBNwmCe35sW1hm36UEuouCnwmSgFWBLMMuNQChf4e2PzADHmhO',	'img/default-child.png',	'ACTIVE',	'0',	0,	'Rgq4ijhzyP9blXPFqNE7sXLgxxtCMdZ3HdSzpRluoOw5H7vsEXqFDaqjGG41',	'2016-09-23 05:33:39',	'2016-09-23 16:30:31'),
(3,	'mom',	'mom@gmail.com',	'mom',	'$2y$10$LwbLBNwmCe35sW1hm36UEuouCnwmSgFWBLMMuNQChf4e2PzADHmhO',	'img/mother.png',	'ACTIVE',	'0',	0,	'D4Plg9tNX63xjLNaZtkWCAsLkrB4K1zC3l2Bf8AwnvM6ncA9pQNmnJjA1CXj',	NULL,	'2016-09-24 09:55:18');

DROP TABLE IF EXISTS `user_homeworks`;
CREATE TABLE `user_homeworks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `homework_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `user_homeworks_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `user_homeworks` (`id`, `user_id`, `homework_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1,	1,	1,	NULL,	NULL,	NULL),
(2,	1,	2,	NULL,	NULL,	NULL),
(3,	1,	3,	NULL,	NULL,	NULL),
(4,	1,	4,	NULL,	NULL,	NULL),
(5,	1,	5,	NULL,	NULL,	NULL),
(6,	1,	6,	NULL,	NULL,	NULL),
(7,	1,	7,	NULL,	NULL,	NULL),
(8,	1,	8,	NULL,	NULL,	NULL),
(9,	1,	9,	NULL,	NULL,	NULL);

-- 2016-09-24 10:09:07
